//
//{block name="backend/emotion/view/detail/elements/base"}
//{$smarty.block.parent}
Ext.define('Shopware.apps.Emotion.view.detail.elements.ctabanner', {

    /**
     * Extend from the base class for the grid elements.
     */
    extend: 'Shopware.apps.Emotion.view.detail.elements.Base',

    /**
     * Create the alias matching with the xtype you defined for your element.
     * The pattern is always 'widget.detail-element-' + xtype
     */
    alias: 'widget.detail-element-emotion-components-ctabanner',

    /**
     * You can define an additional CSS class which will be used for the grid element.
     */
    componentCls: 'emotion--ctabanner',

    /**
     * Define the path to an image for the icon of your element.
     * You could also use a base64 string.
     */
    icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAABm0lEQVRYhe3YvYsTQRiA8V/OKAo2Wlp5WgmCKAjaiWKrvR8LFi5YWMg2WoggBxY6vUw56fwDbCwUS69TQS1VECv1SkXR4nISj2wmOZPsFnlgYWffYeZhhnfeZToh9n5rEUtNC2ymO/C+H2sNecBX/hVaq8rL3xqSEWIPLdyyhVCObq5DiGkJN3ANy3iPiPtVWfyauxAe4upAexn3cBiXpi00cstCTMc2yQxyMcR0cq5COJOJn52WyAY5oc60J8yRE3qaiT+bksdfRgpVZbGKXk34UVUWz+cq1OcKbuNTv/0Zd80gwxgj7ftnzQpWQkzdqix+zkJkg4lO6lnL0MLSsRDK0TqhcYrrlgkxHcJ564V4Nz7gCR7X/SmMJRRiOohT2Ifv+IiXeDcs80JMJ3AL54YMdx2vQ0wXqrJ4NZFQiOkoHuB0TZcfIaY3eIsv2IvjODBqXOsr9gK7xhIKMW3HHdzEthED78CR/jMpO4d9rFuh1S1O8t/UZVkjMrQw7RdCOVonVJdle+ZqMcBQoaosGrt0aN2WdRY3aBn+ALI1VmCKFXhZAAAAAElFTkSuQmCC',

    /**
     * You can override the original `createPreview()` method
     * to create a custom grid preview for your element.
     *
     * @returns { string }
     */
    createPreview: function () {
        var me = this,
            preview = '',
            image = me.getConfigValue('ctabanner_image'),
            style;

        console.log('createPreview', image);
        if (Ext.isDefined(image)) {
            style = Ext.String.format('background-image: url([0]);', image);

            preview = Ext.String.format('<div class="x-emotion-banner-element-preview" style="[0]"></div>', style);
        }

        return preview;
    }
});
//{/block}