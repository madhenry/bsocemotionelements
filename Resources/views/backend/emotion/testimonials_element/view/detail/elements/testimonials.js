//
//{block name="backend/emotion/view/detail/elements/base"}
//{$smarty.block.parent}
Ext.define('Shopware.apps.Emotion.view.detail.elements.testimonials', {

    /**
     * Extend from the base class for the grid elements.
     */
    extend: 'Shopware.apps.Emotion.view.detail.elements.Base',

    /**
     * Create the alias matching with the xtype you defined for your element.
     * The pattern is always 'widget.detail-element-' + xtype
     */
    alias: 'widget.detail-element-emotion-components-testimonials',

    /**
     * You can define an additional CSS class which will be used for the grid element.
     */
    componentCls: 'emotion--testimonials',

    /**
     * Define the path to an image for the icon of your element.
     * You could also use a base64 string.
     */
    // icon: '',

    /**
     * You can override the original `createPreview()` method
     * to create a custom grid preview for your element.
     *
     * @returns { string }
     */
    // createPreview: function () {
    //     var me = this,
    //         preview = '',
    //         image = me.getConfigValue('ctabanner_image'),
    //         style;

    //     console.log('createPreview', image);
    //     if (Ext.isDefined(image)) {
    //         style = Ext.String.format('background-image: url([0]);', image);

    //         preview = Ext.String.format('<div class="x-emotion-banner-element-preview" style="[0]"></div>', style);
    //     }

    //     return preview;
    // }
});
//{/block}