// {namespace name="backend/emotion/bsoc_emotion_elements"}
//{block name="emotion_components/backend/ctabanner"}
Ext.define('Shopware.apps.Emotion.view.components.submenu', {

    /**
     * Extend from the base class for the emotion components
     */
    extend: 'Shopware.apps.Emotion.view.components.Base',

    /**
     * Create the alias matching the xtype you defined in your `createEmotionComponent()` method.
     * The pattern is always 'widget.' + xtype
     */
    alias: 'widget.emotion-components-submenu',

    /**
     * Contains the translations of each input field which was created with the EmotionComponentInstaller.
     * Use the name of the field as identifier
     */
    snippets: {},

    // /**
    //  * The constructor method of each component.
    //  */
    // initComponent: function () {
    //     var me = this;

    //     /**
    //      * Call the original method of the base class.
    //      */
    //     me.callParent(arguments);

    //     /**
    //      * Get single fields you've created with the helper functions in your `Bootstrap.php` file.
    //      */
    //     // me.imageField = me.getForm().findField('ctabanner_image');

    //     /**
    //      * For example you can register additional event listeners on your fields to handle some data.
    //      */
    //     // me.imageField.on('change', Ext.bind(me.onImageChange, me));
    // },

    // onImageChange: function(field, value) {

    // }

});
//{/block}
