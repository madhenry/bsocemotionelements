{block name="widgets_emotion_components_ctabanner_element"}
    <div class="above-wrapper">
        <div class="above-wrapper-img" style="background-image: url('{$Data.ctabanner_image}');"></div>
        <div class="b-wrapper_content fold-text">
            <h1 class="above-h1">
                {$Data.ctabanner_title}
            </h1>
            <a href="{if $Data.ctabanner_link}{$Data.ctabanner_link}{else}{url controller='listing' sCategory=$Data.ctabanner_link_category}{/if}">
                <button class="b-button b-gold-button b-arrow-button">
                    <span>{$Data.ctabanner_button_title}</span>
                </button>
            </a>
        </div><!-- b-wrapper_content -->
        <div class="b-bean-list">
            <div class="b-wrapper_content">
                <ul class="b-bean-ul">
                    <li class="b-bean-item"><img class="b-bean" src="{link file='frontend/_public/src/img/coffee_bean.svg' fullPath}"><span>{$Data.ctabanner_bean_1}</span></li>
                    <li class="b-bean-item"><img class="b-bean" src="{link file='frontend/_public/src/img/coffee_bean.svg' fullPath}"><span>{$Data.ctabanner_bean_2}</span></li>
                    <li class="b-bean-item"><img class="b-bean" src="{link file='frontend/_public/src/img/coffee_bean.svg' fullPath}"><span>{$Data.ctabanner_bean_3}</span></li>
                </ul>
            </div>
        </div><!-- b-bean list -->
    </div>
{/block}