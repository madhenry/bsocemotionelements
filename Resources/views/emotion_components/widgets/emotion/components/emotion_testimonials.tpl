{block name="widgets_emotion_components_testimonials_element"}
    <div class="b-testimonials">
        <div class="b-container">
            <div class="b-testimonials_img">
                <img src="{link file='frontend/_public/src/img/testimonials_img.svg' fullPath}">
            </div>
            <div class="b-testimonials_title">
                <h3>{$Data.testimonials_title}</h3>
                <p>{$Data.testimonials_desc}</p>
            </div>

            <div class="testm--testimonials_carousel">

                <div class="testm_slides-wrapper">
                    <div class="testm_slides">
                        <div class="testm-gradients left-gradient"><img src="{link file='frontend/_public/src/img/gradient-left.png' fullPath}"></div>
                        <div class="testm-gradients right-gradient"><img src="{link file='frontend/_public/src/img/gradient-right.png' fullPath}"></div>

                        <div class="testm-carousel_controls">
                            <span id="testm-slider-prev" class="slider-btn light-version"><img src="{link file='frontend/_public/src/img/arrow_forward_ios_24px.svg' fullPath}" /></span>
                            <span id="testm-slider-next" class="slider-btn light-version"><img src="{link file='frontend/_public/src/img/arrow_forward_ios_24px.svg' fullPath}" /></span>
                        </div>

                        <div class="b-testimonials_carousel">
                            {for $i=1 to 4}
                                {if $Data.{"testimonials_person{$i}_name"}}
                                    <div class="testm-carousel-slider">
                                        <div class="testm--carousel-slide-inner">
                                            <div class="testm--carousel-img">
                                                <div class="testm-man" style="background-image:url('{if $Data.{"testimonials_person{$i}_pic"}}{$Data.{"testimonials_person{$i}_pic"}}{else}{link file='frontend/_public/src/img/john_doe.png' fullPath}{/if}');"></div>
                                            </div>
                                            <div class="testm--carousel-text">
                                                <div class="testm--text-inner">
                                                    <div class="testm--carousel_stars">
                                                        <img class="testm--carousel_star-img" src="{link file='frontend/_public/src/img/stars_row.svg' fullPath}">
                                                    </div>
                                                    <h3>{$Data.{"testimonials_person{$i}_name"}}</h3>
                                                    <p>{$Data.{"testimonials_person{$i}_comment"}}</p>
                                                </div>
                                            </div>
                                        </div><!-- testm-single slide -->
                                    </div><!-- testm--carousel-slider -->
                                {/if}
                            {/for} 
                        </div><!-- testimonials carousel -->

                    </div><!-- testm--courses_slides -->
                </div><!-- testm--courses_slides-wrapper -->

            </div><!-- testimonials carousel -->
        </div><!-- container -->
    </div><!-- testimonials -->
{/block}