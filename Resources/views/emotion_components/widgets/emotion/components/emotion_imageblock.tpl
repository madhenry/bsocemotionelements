{block name="widgets_emotion_components_imageblock_element"}
    {if $Data.imageblock_services}
        <div class="transparent-block{if $Data.imageblock_textleft} m-textleft{/if}">
            <div class="transparent-block_inner flexing">
                <div class="transparent-block_image">
                    {if $Data.imageblock_pic}
                        <img src="{$Data.imageblock_pic}">
                    {/if}
                    {if $Data.imageblock_smallpic}
                        <img class="b-small-img" src="{$Data.imageblock_smallpic}">
                    {/if}
                </div>
                <div class="transparent-block_text">
                    <h3>{$Data.imageblock_title}</h3>
                    <p>{$Data.imageblock_desc}</p>
                    {if $Data.imageblock_button_title}
                        <a href="{if $Data.imageblock_link}{$Data.imageblock_link}{else}{url controller='listing' sCategory=$Data.imageblock_link_category}{/if}">
                            <button class="b-button blk-btn">{$Data.imageblock_button_title}</button>
                        </a>
                    {/if}
                </div><!-- text -->
            </div><!-- inner -->
        </div><!-- single block -->
    {else}
        <div class="{if $Data.imageblock_whitebg}m-whitebg{/if}">
            <div class="b-container flexing b-img_container{if $Data.imageblock_textleft} m-textleft{/if}{if $Data.imageblock_whitebg} m-whitebg{/if}">
                <div class="b-img_block flex">
                    <div class="img-block_inner b-left-padding">
                        {if $Data.imageblock_pic}
                            <img class="b-large-img" src="{$Data.imageblock_pic}">
                        {/if}
                        {if $Data.imageblock_smallpic}
                            <img class="b-small-img" src="{$Data.imageblock_smallpic}">
                        {/if}
                    </div>
                </div><!-- img block -->
                <div class="b-txt_block flex">
                    <div class="b-txt_inner b-left-padding">
                        <h2 class="large_h2">{$Data.imageblock_title}</h2>
                        <p>{$Data.imageblock_desc}</p>
                        {if $Data.imageblock_button_title}
                            <a href="{if $Data.imageblock_link}{$Data.imageblock_link}{else}{url controller='listing' sCategory=$Data.imageblock_link_category}{/if}">
                                <button class="b-button blk-btn">{$Data.imageblock_button_title}</button>
                            </a>
                        {/if}
                    </div>
                </div><!-- b-txt_block -->
            </div><!-- b-container -->
        </div>
    {/if}
{/block}