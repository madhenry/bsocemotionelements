{block name="widgets_emotion_components_textimagesblock_element"}
<div class="two-img-block">
    <div class="b-container flexing">
        {if $Data.textimagesblock_leftpic}
            <div class="two-img_single flex">
                <img src="{$Data.textimagesblock_leftpic}">
            </div>
        {/if}
        {if $Data.textimagesblock_rightpic}
            <div class="two-img_single flex">
                <img src="{$Data.textimagesblock_rightpic}">
            </div>
        {/if}
        <div class="img-block_text">
            <div class="img-block_inner">
                <p>{$Data.textimagesblock_desc}</p>
            </div>
        </div>
    </div>
</div><!-- two-img-block -->
{/block}