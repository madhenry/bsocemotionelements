{block name="widgets_emotion_components_trainer_element"}
    <div class="b-single-memnber" id="{$Data.trainer_name|replace:' ':'-'|lower}">
        <div class="b-single-member_inner flexing">
            <div class="single-member_img flex-short">
                <img class="trainer-profile" src="{$Data.trainer_pic}">
            </div>
            <div class="single-member_info flex">
                <div class="member-info_inner">
                    <h2>{$Data.trainer_name}</h2>
                    <a href="mailto:{$Data.trainer_email}">{$Data.trainer_email}</a>
                    <p>{$Data.trainer_desc}</p>
                    <div class="large-letter_wrap">
                        <span class="large-letter">{$Data.trainer_name|substr:0:1}</span>
                    </div><!-- large letter -->
                </div>
            </div>
        </div>
    </div><!-- single member -->
{/block}