{block name="widgets_emotion_components_separator_element"}
    <div class="b-coffee-cup">
        <div class="cup-img">
            <img src="{if $Data.separator_pic}{$Data.separator_pic}{else}{link file='frontend/_public/src/img/coffee_cup.svg' fullPath}{/if}">
        </div>
    </div><!-- b-coffee-cup -->
{/block}