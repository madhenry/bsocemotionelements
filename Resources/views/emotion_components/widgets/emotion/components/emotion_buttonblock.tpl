{block name="widgets_emotion_components_buttonblock_element"}
    <div class="cta-block">
        <ul class="cta-list">
            {for $i=1 to 5}
                {if $Data.{"buttonblock_title{$i}"}}
                    <li class="cta-item">
                        <a href="{if $Data.{"buttonblock_link{$i}"}}{$Data.{"buttonblock_link{$i}"}}{else}{url controller='listing' sCategory=$Data.{"buttonblock_link_category{$i}"}}{/if}">
                            {$Data.{"buttonblock_title{$i}"}}
                        </a>
                    </li>
                {/if}
            {/for}
        </ul>
    </div>
{/block}