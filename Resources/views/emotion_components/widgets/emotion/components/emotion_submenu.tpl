{block name="widgets_emotion_components_submenu_element"}
    <!-- sub-heading w/ menu  -->
    <div class="b-content-sub-menu">
        <div class="b-container flexing">
            <div class="content-sub-menu_heading">
                <h1>{$Data.submenu_title}</h1>
            </div>
            <div class="content-sub-menu_wrap flex">
                <ul class="sub-navigation" data-currentCategory="{$emotion.categoryIds[0]}">
                    {for $i=1 to 6}
                        {if $Data.{"submenu_link{$i}_title"}}
                            <li class="{if $emotion.categoryIds[0] == $Data.{"submenu_link{$i}_category"}}current-page{/if}">
                                <a href="{if $Data.{"submenu_link{$i}_url"}}{$Data.{"submenu_link{$i}_url"}}{else}{url controller='listing' sCategory=$Data.{"submenu_link{$i}_category"}}{/if}"
                                    data-categoryId="{$Data.{"submenu_link{$i}_category"}}">
                                    {$Data.{"submenu_link{$i}_title"}}
                                </a>
                            </li>
                        {/if}
                    {/for}
                </ul>
            </div>
        </div>
    </div>
    <!-- end sub-heading w/ menu  -->
{/block}