{block name="widgets_emotion_components_trainers_element"}
    <div class="b-best">
        <div class="b-container">
            <div class="b-wrapper_content">
                <img class="b-best_beans-img" src="{link file='frontend/_public/src/img/beans_pic.png' fullPath}">
                <h2 class="b-best_title">{$Data.trainers_title}</h2>
                <div class="b-col_containers flexing">

                    <div class="b-col_box flex">
                        <div class="b-col_box-inner">
                            {$Data.trainers_column1}
                        </div>
                    </div><!-- b-col_box -->
                    <div class="b-col_box flex">
                        <div class="b-col_box-inner">
                            {$Data.trainers_column2}
                        </div>
                    </div><!-- b-col_box -->
                    <div class="b-col_box flex">
                        <div class="b-col_box-inner">
                            {$Data.trainers_column3}
                        </div>
                    </div><!-- b-col_box -->
                    <div class="b-col_box flex">
                        <div class="b-col_box-inner">
                            {$Data.trainers_column4}
                        </div>
                    </div><!-- b-col_box -->

                </div><!-- b-best_container -->
            </div><!-- b-wrapper_content-->

            <div class="b-members">
                <div class="b-member_slides-wrapper">
                    <div class="b-member_slides">
                        <div class="b-member_controls">
                            <span id="member-slider-prev" class="slider-btn"><img src="{link file='frontend/_public/src/img/arrow_forward_ios_24px.svg' fullPath}" /></span>
                            <span id="member-slider-next" class="slider-btn"><img src="{link file='frontend/_public/src/img/arrow_forward_ios_24px.svg' fullPath}" /></span>
                        </div>
                        <div class="b-members-carousel">
                            {assign var="trainers_link" value={url controller='listing' sCategory=$Data.trainers_link_category}}

                            {for $i=1 to 6}
                                <div class="b-member-slider">
                                    <div class="b-member-slide-inner">
                                        <a href="{$trainers_link}#{$Data.{"trainers_trainer{$i}_name"}|replace:' ':'-'|lower}">
                                            <div class="b-member_overlay">
                                                <button>{s name="BsocMeetTrainer"}Meet trainer{/s}</button>
                                            </div>
                                        </a>
                                        <div class="b-member-img" style="background:url('{if $Data.{"trainers_trainer{$i}_pic"}}{$Data.{"trainers_trainer{$i}_pic"}}{else}{link file='frontend/_public/src/img/BSOC_trainer-Christina_DSC02861_x.png' fullPath}{/if}');">
                                        </div>
                                    </div>
                                </div><!-- b-member-slider -->
                            {/for}
                        </div>
                    </div><!-- b-member_slides -->
                </div><!-- b-member_slides-wrapper -->
            </div><!-- b-members -->

        </div> <!-- b-container -->
    </div><!-- b-best -->
{/block}