<?php

namespace BsocEmotionElements\Bootstrap;

use Shopware\Components\Emotion\ComponentInstaller;

class EmotionElementInstaller
{
    /**
     * @var ComponentInstaller
     */
    private $emotionComponentInstaller;

    /**
     * @var string
     */
    private $pluginName;

    /**
     * @param string $pluginName
     * @param ComponentInstaller $emotionComponentInstaller
     */
    public function __construct($pluginName, ComponentInstaller $emotionComponentInstaller)
    {
        $this->emotionComponentInstaller = $emotionComponentInstaller;
        $this->pluginName = $pluginName;
    }

    public function install()
    {
        $this->installBannerElement();
        $this->installTrainersElement();
        $this->installTestimonialsElement();
        $this->installSeparatorElement();
        $this->installImageBlockElement();
        $this->installTextImagesBlockElement();
        $this->installButtonBlockElement();
        $this->installTrainerElement();
        $this->installSubmenuElement();
    }

    private function installBannerElement()
    {
        // Banner
        $ctaBanner = $this->emotionComponentInstaller->createOrUpdate(
            $this->pluginName,
            'ctaBannerElement',
            [
                'name' => 'CTA Banner',
                'xtype' => 'emotion-components-ctabanner',
                'template' => 'emotion_ctabanner',
                'cls' => 'emotion-ctabanner-element',
                'description' => 'A simple CTA Banner element for the shopping worlds.'
            ]
        );

        $ctaBanner->createTextField([
            'name' => 'ctabanner_title',
            'fieldLabel' => 'Title',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $ctaBanner->createMediaField([
            'name' => 'ctabanner_image',
            'fieldLabel' => 'Background Image',
            'supportText' => '',
            'valueField' => 'virtualPath'
        ]);
        
        $ctaBanner->createTextField([
            'name' => 'ctabanner_link',
            'fieldLabel' => 'Link',
            'supportText' => '',
            'allowBlank' => true
        ]);
        
        $ctaBanner->createComboBoxField([
            'name' => 'ctabanner_link_category',
            'store' => 'Shopware.apps.Base.store.Category',
            'displayField' => 'name',
            'valueField' => 'id',
            'fieldLabel' => 'Category link',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $ctaBanner->createTextField([
            'name' => 'ctabanner_button_title',
            'fieldLabel' => 'Button Title',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $ctaBanner->createTextField([
            'name' => 'ctabanner_bean_1',
            'fieldLabel' => 'Bean 1',
            'supportText' => '',
            'allowBlank' => true
        ]);
        $ctaBanner->createTextField([
            'name' => 'ctabanner_bean_2',
            'fieldLabel' => 'Bean 2',
            'supportText' => '',
            'allowBlank' => true
        ]);
        $ctaBanner->createTextField([
            'name' => 'ctabanner_bean_3',
            'fieldLabel' => 'Bean 3',
            'supportText' => '',
            'allowBlank' => true
        ]);
    }

    private function installTrainersElement()
    {
        // Bsoc Trainers
        $bsocTrainers = $this->emotionComponentInstaller->createOrUpdate(
            $this->pluginName,
            'bsocTrainersElement',
            [
                'name' => 'Bsoc Trainers Banner',
                'xtype' => 'emotion-components-trainers',
                'template' => 'emotion_trainers',
                'cls' => 'emotion-trainers-element',
                'description' => '4 columns of text with trainers pics'
            ]
        );

        $bsocTrainers->createTextField([
            'name' => 'trainers_title',
            'fieldLabel' => 'Title',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $bsocTrainers->createTinyMceField([
            'name' => 'trainers_column1',
            'fieldLabel' => 'Column 1',
            'supportText' => '',
            'allowBlank' => true
        ]);
        $bsocTrainers->createTinyMceField([
            'name' => 'trainers_column2',
            'fieldLabel' => 'Column 2',
            'supportText' => '',
            'allowBlank' => true
        ]);
        $bsocTrainers->createTinyMceField([
            'name' => 'trainers_column3',
            'fieldLabel' => 'Column 3',
            'supportText' => '',
            'allowBlank' => true
        ]);
        $bsocTrainers->createTinyMceField([
            'name' => 'trainers_column4',
            'fieldLabel' => 'Column 4',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $bsocTrainers->createComboBoxField([
            'name' => 'trainers_link_category',
            'store' => 'Shopware.apps.Base.store.Category',
            'displayField' => 'name',
            'valueField' => 'id',
            'fieldLabel' => 'Category link',
            'supportText' => '',
            'allowBlank' => true
        ]);

        for ($i=1; $i <= 6; $i++) {
            $bsocTrainers->createMediaField([
                'name' => 'trainers_trainer'.$i.'_pic',
                'fieldLabel' => 'Trainer '.$i.' Image',
                'supportText' => '',
                'valueField' => 'virtualPath',
                'allowBlank' => true
            ]);
            $bsocTrainers->createTextField([
                'name' => 'trainers_trainer'.$i.'_name',
                'fieldLabel' => 'Trainer '.$i.' Name',
                'supportText' => '',
                'allowBlank' => true
            ]);
        }
    }

    private function installTestimonialsElement()
    {
        // Bsoc Testimonials
        $bsocTestimonials = $this->emotionComponentInstaller->createOrUpdate(
            $this->pluginName,
            'bsocTestimonialsElement',
            [
                'name' => 'Bsoc Testimonials',
                'xtype' => 'emotion-components-testimonials',
                'template' => 'emotion_testimonials',
                'cls' => 'emotion-testimonials-element',
                'description' => 'Text block with testimonials slider'
            ]
        );

        $bsocTestimonials->createTextField([
            'name' => 'testimonials_title',
            'fieldLabel' => 'Title',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $bsocTestimonials->createTinyMceField([
            'name' => 'testimonials_desc',
            'fieldLabel' => 'Description',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $bsocTestimonials->createMediaField([
            'name' => 'testimonials_person1_pic',
            'fieldLabel' => 'Person 1 Image',
            'supportText' => '',
            'valueField' => 'virtualPath',
            'allowBlank' => true
        ]);
        $bsocTestimonials->createTextField([
            'name' => 'testimonials_person1_name',
            'fieldLabel' => 'Person 1 Name',
            'supportText' => '',
            'allowBlank' => true
        ]);
        $bsocTestimonials->createTextField([
            'name' => 'testimonials_person1_comment',
            'fieldLabel' => 'Person 1 Comment',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $bsocTestimonials->createMediaField([
            'name' => 'testimonials_person2_pic',
            'fieldLabel' => 'Person 2 Image',
            'supportText' => '',
            'valueField' => 'virtualPath',
            'allowBlank' => true
        ]);
        $bsocTestimonials->createTextField([
            'name' => 'testimonials_person2_name',
            'fieldLabel' => 'Person 2 Name',
            'supportText' => '',
            'allowBlank' => true
        ]);
        $bsocTestimonials->createTextField([
            'name' => 'testimonials_person2_comment',
            'fieldLabel' => 'Person 2 Comment',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $bsocTestimonials->createMediaField([
            'name' => 'testimonials_person3_pic',
            'fieldLabel' => 'Person 3 Image',
            'supportText' => '',
            'valueField' => 'virtualPath',
            'allowBlank' => true
        ]);
        $bsocTestimonials->createTextField([
            'name' => 'testimonials_person3_name',
            'fieldLabel' => 'Person 3 Name',
            'supportText' => '',
            'allowBlank' => true
        ]);
        $bsocTestimonials->createTextField([
            'name' => 'testimonials_person3_comment',
            'fieldLabel' => 'Person 3 Comment',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $bsocTestimonials->createMediaField([
            'name' => 'testimonials_person4_pic',
            'fieldLabel' => 'Person 4 Image',
            'supportText' => '',
            'valueField' => 'virtualPath',
            'allowBlank' => true
        ]);
        $bsocTestimonials->createTextField([
            'name' => 'testimonials_person4_name',
            'fieldLabel' => 'Person 4 Name',
            'supportText' => '',
            'allowBlank' => true
        ]);
        $bsocTestimonials->createTextField([
            'name' => 'testimonials_person4_comment',
            'fieldLabel' => 'Person 4 Comment',
            'supportText' => '',
            'allowBlank' => true
        ]);
    }

    private function installSeparatorElement()
    {
        // Bsoc Separator
        $bsocSeparator = $this->emotionComponentInstaller->createOrUpdate(
            $this->pluginName,
            'bsocSeparatorElement',
            [
                'name' => 'Bsoc Separator',
                'xtype' => 'emotion-components-separator',
                'template' => 'emotion_separator',
                'cls' => 'emotion-separator-element',
                'description' => 'Coffee cup separator'
            ]
        );

        $bsocSeparator->createMediaField([
            'name' => 'separator_pic',
            'fieldLabel' => 'Alternative Image',
            'supportText' => '',
            'valueField' => 'virtualPath',
            'allowBlank' => true
        ]);
    }

    private function installImageBlockElement()
    {
        // Bsoc Separator
        $bsocImageBlock = $this->emotionComponentInstaller->createOrUpdate(
            $this->pluginName,
            'bsocImageBlockElement',
            [
                'name' => 'Bsoc ImageBlock',
                'xtype' => 'emotion-components-imageblock',
                'template' => 'emotion_imageblock',
                'cls' => 'emotion-imageblock-element',
                'description' => 'Text blocks with images on either side'
            ]
        );

        $bsocImageBlock->createTextField([
            'name' => 'imageblock_title',
            'fieldLabel' => 'Title',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $bsocImageBlock->createTinyMceField([
            'name' => 'imageblock_desc',
            'fieldLabel' => 'Description',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $bsocImageBlock->createMediaField([
            'name' => 'imageblock_pic',
            'fieldLabel' => 'Large Image',
            'supportText' => '',
            'valueField' => 'virtualPath',
            'allowBlank' => true
        ]);

        $bsocImageBlock->createMediaField([
            'name' => 'imageblock_smallpic',
            'fieldLabel' => 'Small Image',
            'supportText' => '',
            'valueField' => 'virtualPath',
            'allowBlank' => true
        ]);

        $bsocImageBlock->createTextField([
            'name' => 'imageblock_link',
            'fieldLabel' => 'External button Link',
            'supportText' => '',
            'allowBlank' => true
        ]);
        
        $bsocImageBlock->createComboBoxField([
            'name' => 'imageblock_link_category',
            'store' => 'Shopware.apps.Base.store.Category',
            'displayField' => 'name',
            'valueField' => 'id',
            'fieldLabel' => 'Category link',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $bsocImageBlock->createTextField([
            'name' => 'imageblock_button_title',
            'fieldLabel' => 'Button Title',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $bsocImageBlock->createCheckboxField([
            'name' => 'imageblock_textleft',
            'fieldLabel' => 'Text on the left side?',
            'supportText' => ''
        ]);

        $bsocImageBlock->createCheckboxField([
            'name' => 'imageblock_whitebg',
            'fieldLabel' => 'White background?',
            'supportText' => ''
        ]);

        $bsocImageBlock->createCheckboxField([
            'name' => 'imageblock_services',
            'fieldLabel' => 'Alternative style with black border?',
            'supportText' => ''
        ]);
    }

    private function installTextImagesBlockElement()
    {
        // Bsoc Text images block
        $bsocTextImagesBlock = $this->emotionComponentInstaller->createOrUpdate(
            $this->pluginName,
            'bsocTextImagesBlockElement',
            [
                'name' => 'Bsoc Text with Background Images',
                'xtype' => 'emotion-components-textimagesblock',
                'template' => 'emotion_textimagesblock',
                'cls' => 'emotion-textimagesblock-element',
                'description' => 'Text block with background images on both side'
            ]
        );

        $bsocTextImagesBlock->createTinyMceField([
            'name' => 'textimagesblock_desc',
            'fieldLabel' => 'Description',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $bsocTextImagesBlock->createMediaField([
            'name' => 'textimagesblock_leftpic',
            'fieldLabel' => 'Left Image',
            'supportText' => '',
            'valueField' => 'virtualPath',
            'allowBlank' => true
        ]);

        $bsocTextImagesBlock->createMediaField([
            'name' => 'textimagesblock_rightpic',
            'fieldLabel' => 'Right Image',
            'supportText' => '',
            'valueField' => 'virtualPath',
            'allowBlank' => true
        ]);
    }

    private function installButtonBlockElement()
    {
        // Bsoc Text images block
        $bsocButtonBlock = $this->emotionComponentInstaller->createOrUpdate(
            $this->pluginName,
            'bsocButtonBlockElement',
            [
                'name' => 'Bsoc Buttons Block',
                'xtype' => 'emotion-components-buttonblock',
                'template' => 'emotion_buttonblock',
                'cls' => 'emotion-buttonblock-element',
                'description' => 'Text block with background images on both side'
            ]
        );

        for ($i=1; $i < 6; $i++) {
            $bsocButtonBlock->createTextField([
                'name' => 'buttonblock_title'.$i,
                'fieldLabel' => 'Title '.$i,
                'supportText' => '',
                'allowBlank' => true
            ]);
    
            $bsocButtonBlock->createTextField([
                'name' => 'buttonblock_link'.$i,
                'fieldLabel' => 'External button Link '.$i,
                'supportText' => '',
                'allowBlank' => true
            ]);
            
            $bsocButtonBlock->createComboBoxField([
                'name' => 'buttonblock_link_category'.$i,
                'store' => 'Shopware.apps.Base.store.Category',
                'displayField' => 'name',
                'valueField' => 'id',
                'fieldLabel' => 'Category link '.$i,
                'supportText' => '',
                'allowBlank' => true
            ]);
        }
    }

    private function installTrainerElement()
    {
        // Bsoc Text images block
        $bsocTrainer = $this->emotionComponentInstaller->createOrUpdate(
            $this->pluginName,
            'bsocTrainerElement',
            [
                'name' => 'Bsoc Trainer Block',
                'xtype' => 'emotion-components-trainer',
                'template' => 'emotion_trainer',
                'cls' => 'emotion-trainer-element',
                'description' => 'Trainer description element'
            ]
        );

        $bsocTrainer->createTextField([
            'name' => 'trainer_name',
            'fieldLabel' => 'Name',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $bsocTrainer->createTextField([
            'name' => 'trainer_email',
            'fieldLabel' => 'Email',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $bsocTrainer->createTinyMceField([
            'name' => 'trainer_desc',
            'fieldLabel' => 'Description',
            'supportText' => '',
            'allowBlank' => true
        ]);

        $bsocTrainer->createMediaField([
            'name' => 'trainer_pic',
            'fieldLabel' => 'Image',
            'supportText' => '',
            'valueField' => 'virtualPath',
            'allowBlank' => true
        ]);
    }

    private function installSubmenuElement()
    {
        // Bsoc Text images block
        $bsocSubmenu = $this->emotionComponentInstaller->createOrUpdate(
            $this->pluginName,
            'bsocSubmenuElement',
            [
                'name' => 'Bsoc submenu Block',
                'xtype' => 'emotion-components-submenu',
                'template' => 'emotion_submenu',
                'cls' => 'emotion-submenu-element',
                'description' => 'Submenu with title and links'
            ]
        );

        $bsocSubmenu->createTextField([
            'name' => 'submenu_title',
            'fieldLabel' => 'Title',
            'supportText' => '',
            'allowBlank' => true
        ]);

        for ($i=1; $i <= 6; $i++) {
            $bsocSubmenu->createTextField([
                'name' => 'submenu_link'.$i.'_title',
                'fieldLabel' => 'Link Title '.$i,
                'supportText' => '',
                'allowBlank' => true
            ]);
    
            $bsocSubmenu->createTextField([
                'name' => 'submenu_link'.$i.'_url',
                'fieldLabel' => 'External link '.$i,
                'supportText' => '',
                'allowBlank' => true
            ]);
            
            $bsocSubmenu->createComboBoxField([
                'name' => 'submenu_link'.$i.'_category',
                'store' => 'Shopware.apps.Base.store.Category',
                'displayField' => 'name',
                'valueField' => 'id',
                'fieldLabel' => 'Category link '.$i,
                'supportText' => '',
                'allowBlank' => true
            ]);
        }
    }
}
